
var initial_input = [
               [0,1,2,3,4,5],
               [5,0,3,4,2,1],
               [2,5,4,3,1,0],
               [4,5,2,0,1,3],
               [4,1,0,5,3,2],
               [1,2,5,0,3,4],
               [2,0,4,5,1,3],
            ];

var input = [];

var output = [ -1, -1, -1, -1, -1, -1, -1 ];

// rotates ex such that color goes in position pos 
var rotate_ex = function ( ex, pos, color )
{
	var i=0;
	var copy_ex = [];
	for ( var j=0; j<6; ++j )
	{
		copy_ex.push ( ex[j] );
		if ( ex[j] == color ) i = j;
	}
	for ( var j=0; j<6; ++j )
	{
		ex[j] = copy_ex[ (j + i - pos + 6)%6 ];
	}
}

// positions input[from] in output[to], satisfying the constraint imposed by the central exagone
var position = function ( from, to )
{
	if ( to != 6 )
	{
		rotate_ex ( input[from], (to+3)%6, input[output[6]][to] );
	}
	output[to] = from;
}

// swaps input[i] with input[j]
var swap_input_ex = function ( i, j )
{
	var copy_ex = input[i];
	input[i] = input[j];
	input[j] = copy_ex;
}

// checks if the exagon in position pos satisfies the constraints imposed by the neighbour exagones
var border_constraints_are_satisfied = function ( pos )
{
	var satisfied = true;
	var ex = input[output[pos]];
	if ( output[ (pos+1)%6 ] != -1 )
	{
		var other_ex = input[ output[(pos+1)%6] ]
		satisfied = satisfied && ( other_ex[(pos+5)%6] == ex[(pos+2)%6] )
	}
	if ( output[ (pos+5)%6 ] != -1 )
	{
		var other_ex = input[ output[(pos+5)%6] ]
		satisfied = satisfied && ( other_ex[(pos+1)%6] == ex[(pos+4)%6] )
	}
	return satisfied;
}

// assumes output from N to 6 already positioned, and try to positon the other exagones in the border, from N to 0.
// returns true if all the exagones in the border are correctly positioned (the problem is solcved).
// returns false otherwise
var solve_border = function ( N )
{
	//~ debug ("called solve_border N="+N);
	//~ debug ("input"+input_str());
	if ( N<0 )
	{
		//~ debug ("return value: true");
		return true;
	}
	for ( var i=0; i<=N; ++i )
	{
		swap_input_ex ( i, N );
		position ( N, N );
		//~ debug ("positioned "+input[N]+" in pos "+N);
		//~ debug ("output "+output);
		//~ debug ("border contstraints:"+ border_constraints_are_satisfied (N))
		if ( border_constraints_are_satisfied (N) )
		{
			if ( solve_border ( N-1 ) )
			{
				//~ debug ("return value: true");
				return true;
			}
		}
		swap_input_ex ( N, i );
	}
	output[N] = -1;
	//~ debug ("return value: false");
	return false;
}

//try solving the whole problem. Returns false if it is unsolvable, else it returns true and the output is in the global variable output. 
var solve = function ()
{
	var solved = false;
	var i=0;
	output = [ -1, -1, -1, -1, -1, -1, -1 ];
	while (i<=6 && !solved)
	{
		swap_input_ex ( i, 6 );
		position ( 6, 6 );
		//~ debug ("\n\nPOSITIONED CENTER i="+i);
		solved = solve_border ( 5 );
		if ( ! solved )	swap_input_ex ( 6, i );
		++i;
	}
	return solved;
}

// returns a printable representation of input
var input_str = function ()
{
	var ret = "\n"
	for (var i=0; i<7; ++i)
	{
		ret += i + " -> "+input[i] + "\n";
	}
	return ret;
}

//TEST
//~ ex = input[0];
//~ debug("BEFORE");
//~ debug(ex);
//~ rotate_ex ( ex, 3, 5 );
//~ debug("AFTER ROTATION");
//~ debug(ex);
//~ position (4, 6);
//~ position (0, 0);
//~ position (2, 1);
//~ debug ("output:"+ output);
//~ debug ("rotated input:"+ input_str());
//~ debug ("border contstraints:"+ border_constraints_are_satisfied (0));
//~ solve()
