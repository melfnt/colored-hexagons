
var color_map = ["#00FF00", "#FFFF00", "#0000FF", "#FF0000", "#000000", "#FFFFFF"];
var output_hex_pos = [ 
                        {"x":"92px", "y":"10px"},
                        {"x":"184px", "y":"64px"},
                        {"x":"184px", "y":"170px"},
                        {"x":"92px", "y":"223px"},
                        {"x":"0px", "y":"170px"},
                        {"x":"0px", "y":"64px"},
                        {"x":"92px", "y":"117px"}
                     ];

var edit_eye = function ()
{
	var id_split = this.id.split("_");
	var i = 1*id_split[1];
	var j = 1*id_split[2];
	console.log ("initial input before:", initial_input[i][j]);
	initial_input[i][j] = ( initial_input[i][j] + 1 ) % 6;
	console.log ("initial input after:", initial_input[i][j]);
	this.style.backgroundColor = color_map [ initial_input[i][j] ];
}

var create_hexagon = function ( i, editable )
{
	var hex = document.createElement ("div");
	hex.className = "hex-container";
	var hex_p1 = document.createElement ("div");
	hex_p1.className = "hex-part-1";
	var hex_p2 = document.createElement ("div");
	hex_p2.className = "hex-part-2";
	var hex_p3 = document.createElement ("div");
	hex_p3.className = "hex-part-3";
	hex.appendChild ( hex_p1 );
	hex.appendChild ( hex_p2 );
	hex.appendChild ( hex_p3 );
	
	for ( var j=0; j<6; ++j )
	{
		var eye = document.createElement ("div");
		eye.className = "eye eye"+j;
		if (editable)
		{
			eye.id = "eye_"+i+"_"+j;
			eye.addEventListener ("click", edit_eye);
		}
		eye.style.backgroundColor = color_map [ input[i][j] ];
		hex.appendChild (eye);
	}
	
	//~ var eye = document.createElement ("div");
	//~ eye.className = "eye0";
	//~ hex.appendChild (eye);
	
	return hex;
}

var set_initial_input = function ()
{
	for ( var i=0; i<7; ++i )
	{
		initial_input[i] = []
		for ( var j=0; j<7; ++j )
		{
			initial_input[i][j] = input[i][j];
		}
	}
}

var set_input = function ()
{
	for ( var i=0; i<7; ++i )
	{
		input[i] = []
		for ( var j=0; j<7; ++j )
		{
			input[i][j] = initial_input[i][j];
		}
	}
}

var generate_random_input = function ()
{
	set_input();
	for ( var i=0; i<7; ++i )
	{
		var j = i + Math.floor ( Math.random () * (7-i) );
		swap_input_ex ( i, j );
		rotate_ex ( input[i], Math.floor ( Math.random () * 6 ), Math.floor ( Math.random () * 6 ) );
	}
	var input_container = document.getElementById ("input-container");
	input_container.innerHTML = "";
	for ( var i=0; i<7; ++i )
	{
		var hex = create_hexagon ( i, true );
		input_container.appendChild ( hex );
	}
	set_initial_input ();
}

var position_output = function ()
{
	var output_container = document.getElementById ("output-container");
	output_container.innerHTML = "";
	for ( var i=0; i<7; ++i )
	{
		var hex = create_hexagon ( i, false );
		hex.style.top = output_hex_pos[i].y;
		hex.style.left = output_hex_pos[i].x;
		hex.style.position = "absolute";
		output_container.appendChild ( hex );
	}
}

var solve_and_measure_time = function ()
{
	set_input();
	var start = new Date().getTime();
	var solved = solve();
	var elapsed = new Date().getTime() - start;
	if ( solved )
	{
		document.getElementById ("message-container").innerHTML = "puzzle solved in "+elapsed+" millisecond(s)";
		position_output ();
	}
	else
	{
		var output_container = document.getElementById ("output-container").innerHTML = "";
		document.getElementById ("message-container").innerHTML = "unsolvable puzzle ( time elapsed: "+elapsed+" millisecond(s) )";
	}
}

var init = function ()
{
	generate_random_input ();
	solve_and_measure_time ();
	document.getElementById("randinput_btn").addEventListener ("click", generate_random_input);
	document.getElementById("solve_btn").addEventListener ("click", solve_and_measure_time);
	document.getElementById("reset_btn").addEventListener ("click", function () {location.reload()});
}

window.addEventListener ('load', init);

